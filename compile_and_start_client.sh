rm -r out/
mkdir out/


cat banner.txt
printf "\n"
javac -cp lib/gson-2.8.5.jar:lib/servlet-api.jar:lib/mysql-connector-java-8.0.13.jar src/com/mael/*.java -d out/
cd out/


echo "Client CLI ou GUI ? "
read INPUT_STRING
INPUT_STRING=$(echo "$INPUT_STRING" | tr '[:upper:]' '[:lower:]')
case $INPUT_STRING in
	cli)
		echo "CLI OK"
		java -cp :../lib/gson-2.8.5.jar:../lib/mysql-connector-java-8.0.13.jar com.mael.MainClient
		;;
	gui)
		echo "GUI OK"
		java -cp :../lib/gson-2.8.5.jar:../lib/mysql-connector-java-8.0.13.jar com.mael.testGUI
		;;
	*)
		echo "Tapez CLI ou GUI"
		;;
esac
