﻿# Projet POO - Clavardage

![enter image description here](https://img.shields.io/badge/Connexion-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Decouverte_des_utilisateurs-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Echange_de_messages_textes-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Echange_de_fichiers-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Récupérer_l'historique_des_messages-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Changer_de_pseudo-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Se_déconnecter-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Notification_de_toute_action_sur_le_réseau-OK-brightgreen.svg)
![enter image description here](https://img.shields.io/badge/Interface_graphique-OK-brightgreen.svg)

Réalisation par *Léo PICOU* et *Maël Galliot* - 4IR-SC

# Quick start


*Préambule : Se référer au manuel d'utilisation pour plus de détails concernant l'installation et le lancement de l'application.*


Pour télécharger l'application, faites un git clone sur le projet : `git clone https://gitlab.com/TlcTac/projetpoo.git`.
Pour le bon fonctionnement de l'application, vous devez :

1. Lancer le serveur sur la machine A
2. Lancer le client sur la machine B,C,D...

Ne lancez pas le client sur la même machine que le serveur, des problèmes vont survenir, et ils ne sont pas considéré comme des bugs
car ce n'est pas une utilisation normale de l'application. 

## Serveur

Pour lancer le serveur, un script est disponible a la racine du projet : `start_server_clavardage`. 

Pour s’exécuter, Apache Tomcat a besoin de ces deux variables d'environnement :

- JAVA_HOME : Doit pointer vers votre JDK
- JRE_HOME : Doit pointer vers votre JRE

Par exemple sous Windows pour une installation de java par défaut : 

	JAVA_HOME=C:\Program Files\Java\jdk1.8.x.x
	JRE_Home=C:\Program Files\Java\jre1.8.x.x

Sous linux, ces variables sont normalement définies par défaut. Toujours sous Linux, n'hésitez pas a faire un petit coup de `chmod` sur le projet le pour rendre exécutable/écrivable. 

Cela suffit pour démarrer sans accroc le serveur, les sources ont déjà été compilés et insérés dans une webapps que vous trouverez dans apache/webapps/clavardage. 

Lancement et démonstration des fonctionnalités du serveur :

http://sendvid.com/hw2vebvf


## Client

Démonstration du client et de son fonctionnement :

- GUI  : http://sendvid.com/x24zyv2y
- CLI : http://sendvid.com/g2h0gbv2



### MySQL
Il est nécessaire d'avoir installé un serveur MySQL pour activer la persistance. Une base de données `clavardage` préalable doit avoir été créé dans MySQL. Deux tables doivent composer cette base de données, `Users` et `Messages`.  Voici toutes les requêtes  SQL a lancer pour configurer correctement la persistance :

	CREATE DATABASE clavardage;
	USE clavardage;	
		
	CREATE TABLE Users (
		IPDest VARCHAR(15),
		Pseudo VARCHAR(60),
		PRIMARY KEY (IPDest)
	);
	
	CREATE TABLE Messages (
		IPDest VARCHAR(15),
		Message VARCHAR(500),
		Date DATETIME,
		FOREIGN KEY (IPDest) REFERENCES Users(IPDest) 
	);

Pour plus d'informations sur ces tables, voir la section "Base de données SQL" dans Architecture du manuel d'utilisation.

### Application

Pour lancer le client, deux .jar sont disponibles dans /out :
- CLI.jar : Permet de lancer le client en mode console, moins agréable mais plus robuste.
- GUI.jar : Permet de lancer le client avec l'interface graphique. 

Au démarrage, on demande a l'utilisateur :

- L'adresse du serveur.
- Les identifiants pour se connecter a la base de données MySQL.

Si l'adresse du serveur est erronée, alors l'application ne démarrera pas, en revanche si MySQL n'est pas correctement configuré, l'application s’exécutera, mais sans persistance des messages.

Un autre script est présent a la racine du projet `compile_and_start_client` qui vas compiler les sources présentes dans src/ et exécuter au choix le client CLI ou GUI.
Le bytecode est généré dans /out. 



