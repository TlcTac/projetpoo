﻿

# Fonctionnalités 

Toutes les fonctionnalités du cahier des charges sont présentes.

- Se connecter
- Récupérer les utilisateurs connectés
- Envoyer un message ou un fichier à l'un d'eux
- Récupérer l'historique des messages, avec horodatage
- Changer de pseudo
- Voir les utilisateurs connectés
- Se déconnecter

Dans la suite de ce manuel, nous allons détailler certaines fonctionnalités, ainsi que l'architecture de notre système de clavardage.

# Architecture

## Diagramme de classe
![Diagramme de classe](https://i.imgur.com/fHrJoeT.png)<center><em>Diagramme de classe  </center></em>
Le diagramme de classe a été simplifié pour être lisible, des liens ont été omis, ce qui explique les classes non-reliées en bas du diagramme.

Présentation des classes :
- Commune au client et au serveur :
	- `User` : Représente un utilisateur. Les utilisateurs sont **définis grâce à leurs adresses IP**, elle ne doit pas changer pour le bon fonctionnement de l'application. Ce choix a été motivé par l'environnement dans lequel l'application s’exécute, où chaque utilisateur est sur un PC fixe, donc avec une adresse IP supposé fixe. 
	- `Message` : Représente un message. Un objet Message peut contenir du texte, un fichier, ou bien une liste d'utilisateur. 
	- `UserDatabase` : Contient la liste locale des utilisateurs connectés. Cette liste est mise à jour régulièrement en communiquant avec le serveur. 

- Spécifique au client :

	- `Session` : Représente une conversation avec un utilisateur distant. Dans un objet Session, il y a une liste d'objet Message. 
	- `SessionManager` : Contient la liste des conversations avec tout les utilisateurs connectés et déconnectés. Dans cette classe, il y a une liste statique d'objet Session. Cette classe fait aussi le lien avec le base de donnée SQL pour récupérer les anciens messages.
	- `SQLDB` : Responsable de la persistance des messages, cette classe établit la connexion avec la BDD SQL. Son fonctionnement est décrit en détail plus loin.
	- `ThreadListener` et `ThreadListenerSonTCP` : Responsable des connexions TCP entrantes, ces classes permettent de traiter les messages reçus en TCP sur le port 5001.
	
	- `HTTPNotifier` : Cette classe notifie le serveur des actions de l'utilisateur, comme par exemple une déconnexion, une connexion ou un changement de pseudo. Elle permet aussi de récupérer la liste des utilisateurs actifs en interrogeant le serveur de présence. 

- Spécifique au serveur :
 	- `ServletServer` : Contient la servlet du serveur.  C'est le point d'entrée des requêtes HTTP GET. Le traitement est ensuite délégué a un objet ServerManager.
 	- `ServerManager` : Traite les notifications reçues. Les notifications sont les même que celles envoyés par HTTPNotifier, c'est a dire changement de pseudo, connexion ou déconnexion, ou demande de base de donnée.
  	- `ThreadSenderServer` : Responsable de l'envoie en TCP de message du serveur vers les différents clients connectés. 
  


## Diagramme d'activité

Nous allons à présent décrire le comportement du système à l'aide d'un diagramme d'activité. Nous commençons par le client.

### Client


![Diagramme d'activité](https://i.imgur.com/1PjshzN.jpg)<center><em>Diagramme d'activité du client  </center></em>

Toujours dans une optique de visibilité, toutes les fonctions ne sont pas montrées ici, et beaucoup sont volontairement abstraites dans le but de ne pas alourdir le diagramme. 

#### Initialisation d'un client
Au démarrage, le client vérifie la connectivité avec la BDD SQL locale, et affiche un message pour prévenir l'utilisateur de la réussite ou de l'échec de cette connexion :
- Si c'est un échec, toutes les fonctions de persistance des messages sont désactivées, les conversations seront effacées lors de la fermeture de l'application.
-  Si c'est une réussite, alors toutes les conversations sont chargées dans l'application dans la classe SessionManager dans un objet statique sessionList. L'ajout se déroule de la même manière que lorsqu'on crée une session avec un utilisateur, il n'y a aucun moyen de savoir si une session a été chargé depuis la BDD SQL ou si elle a été créé en cours de route. Cette abstraction permet au client de fonctionner sans problème au cas où la BDD SQL n'est pas disponible.

Le client démarre le thread d'écoute TCP qui lui permettra de recevoir les listes utilisateurs. Ce même thread permettra aussi de recevoir les messages des autres utilisateurs une fois que le client sera connecté.

L'utilisateur local doit rentrer l'URL du serveur de présence, on teste ensuite la connectivité avec ce dernier, si ça ne fonctionne pas alors on notifie l'utilisateur, sinon on passe à l'étape d'après.

Le client demande au serveur de présence la liste des utilisateurs connectés grâce à la classe `HTTPNotifier` . Une fois que le serveur a répondu, on demande à l'utilisateur de choisir un pseudo. Ce pseudo doit être valide, c'est à dire :

- Contenir exclusivement des caractères alphanumériques, espace, tiret et underscore.
- Ne pas contenir d'espace, tiret ou underscore plusieurs fois d'affilés.
- Ne pas finir par un espace, tiret ou underscore.
- Faire moins de 60 caractères. 
- Ne dois pas déjà exister dans la liste des pseudo connectés.

Cela se traduit par cette expression rationnelle :

`[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*`

Une fois le pseudo choisi, le client notifie le serveur de présence de la connexion d'un nouvel utilisateur et lui envoie le pseudo correspondant. Le serveur met alors sa liste d'utilisateur à jour et envoie cette table à tous les autres utilisateurs connectés *(Le comportement du serveur sera explicité dans la section suivante)*. Le client reçoit alors la table dans lequel il est lui même inclus, et met à jour sa table locale d'utilisateurs.
**NB :** Cette table locale est juste une version en cache temporaire de la liste du serveur, elle ne fait pas autorité, en cas de nécessité, le client demande un rafraîchissement. 

Le client est désormais opérationnel, et peut maintenant communiquer avec les autres utilisateurs. En fonction de l'interaction de l'utilisateur, l'application enverra les notifications suivantes :

- *PSEUDO:(nouveauPseudo)* : pour signaler un changement de pseudo.
- *fetchDatabase* : pour récupérer la dernière version de la liste des utilisateurs auprès du serveur. 
- *DISCONNECTED:(pseudo)* : pour signaler la déconnexion du client. 

Ces notifications sont envoyées en clair avec la méthode HTTP GET grâce aux méthodes statiques de la classe `HTTPNotifier`. Pour cette raison, vous pouvez saisir manuellement l'adresse du serveur sur le navigateur et passer en paramètre d'une requête GET la notification que vous souhaitez.

#### Réception d'une table utilisateur

Dès qu'un client reçoit une table d'utilisateur, il met à jour sa table locale inconditionnellement. Il peut recevoir une table de la part du serveur dans deux cas de figure :

 -  Suite à une notification qu'il a lui même envoyé.
 - Suite à une notification qu'un autre client a envoyé.

Un client ne recevra jamais de table d'un autre client, puisque le modèle peer-to-peer a été abandonné au profit d'une gestion de présence centralisée. 

Dans le cadre de l'interface graphique, pour être sur que la liste utilisateur affichée à l'écran est bien à jour, une demande `fetchDatabase` est envoyé au serveur toutes les 10 sec, et l'interface est mise à jour en conséquence.
Pour le CLI, au moment d'envoyer un message, on fetch et affiche la liste utilisateur. Par conséquent, la CLI génère beaucoup moins de trafic. 


#### Communication avec les autres clients

Les clients communiquent entre eux avec le protocole TCP sans couche supérieure, et s'échange des objets `Message`. Cet objet peut être rempli avec :

- Du texte
- Une liste utilisateur `List<User>`
- Un fichier

##### Recevoir un message
Lorsqu'un utilisateur reçoit un message, il est notifié, et ce message est ajouté dans la session correspondant à l'utilisateur expéditeur. Dans le même temps, le message est ajouté dans la base de données SQL.
##### Envoyer un message
Lorsqu'un utilisateur envoie un message, ce dernier est stocké dans la session correspondant à l'utilisateur destinataire. Dans le même temps, le message est ajouté dans la base de données SQL.
##### Échange de fichiers
Un fichier étant un objet Message comme un autre, il sera traité par le système de la même manière que dans les deux derniers paragraphes. Pour ce qui est de la réception d'un fichier, un **dossier est créé** à ce moment la pour enregistrer le fichier à l'intérieur. Ce dossier se situe à la racine de l’exécutable et se nomme `filesReceived`.  
**NB :** On ne stocke dans une conversation que le nom du fichier et pas son contenu pour des raisons évidentes de taille.

#### Changement de pseudo
Lorsque l'utilisateur souhaite changer de pseudo, une vérification de validité est appliqué pour vérifier que le pseudo est conforme aux exigences de l'application. Ces vérifications sont les mêmes que pour le premier choix de pseudo lors de la connexion *(cf.  Initialisation d'un client )*. Si le pseudo est valide, alors le client notifie le serveur qui à son tour notifiera les autres clients.

#### Déconnexion

Lors de la déconnexion, une notification est simplement envoyé au serveur, et l'application se ferme ensuite.

### Base de données SQL

Comme expliqué précédemment, l'application interagit avec la BDD dans 3 cas de figures :
- Initialisation de l'application : l'application récupère les conversations sauvegardées.
- Envoie d'un message : l'application sauvegarde le message dans la base de données.
- Réception d'un message : l'application sauvegarde le message dans la base de données.


La BDD est composée de deux tables étroitement liées ensemble :
<center> <img src="https://i.imgur.com/lt79esO.png" alt="Table Users"> <img src="https://i.imgur.com/csM1J7t.png" alt="Table Messages"></center>
<center><em>Table Users  et Messages </center> </em>


La table `Users` fait la correspondance entre une adresse IP et le **dernier** pseudo en date pour cette IP. IPDest est une clé primaire car elle est nécessairement unique à **UN** utilisateur, donc le pseudo peut changer mais pas l'adresse IP. Pour cette raison, le changement de pseudo d'un utilisateur n'aura pas d'impact sur l'enregistrement des messages.
La table `Messages` fait la correspondance entre une IP et un message échangé. IPDest est une clé étrangère, qui fait bien sûr référence a la clé primaire de la table `Users`. La colonne `Message`de cette table contient le message qui a été reçu ou envoyé, auquel on ajoute le pseudo. **Pour chaque message** échangé, le pseudo de l’expéditeur est rajouté au contenu du message, par conséquent, pour une adresse IP donnée on peut connaître l’expéditeur et le contenu :

	<PSEUDO> : <MESSAGE>


Lorsqu'on souhaite afficher la conversation avec un utilisateur distant A, il suffit donc de sélectionner  tous les messages où $IPDest=IP_A$ pour récupérer l'ensemble des messages et leurs dates. Voila la requête : 

![Reqûete SQL](https://i.imgur.com/stE58KB.png)<center><em>Execution de la requête SQL sous le logiciel <a href="https://en.wikipedia.org/wiki/MySQL_Workbench">MySQL Workbrench</a> </center> </em>


Supposons que l'utilisateur local possède l'adresse `192.168.0.1`, et le pseudo qu'il ait choisi soit "Michel". On peut voir donc que ces deux éléments apparaissent dans la table `Users`. 
Dans la table `Messages` :

 <center><img src="https://i.imgur.com/csM1J7t.png" alt="Table Messages"></img></center>
 
On aperçoit des choses intéressantes :
- Deux lignes sont identiques, l'utilisateur s'est envoyé un message à lui même, le message a été ajouté lors de l'envoie, et lors de la réception.
- L'utilisateur local a envoyé un message à 192.168.0.2 et ce dernier a répondu.
- L'utilisateur local a envoyé un message à 192.168.0.4 et ce dernier n'a pas encore répondu.
- L'utilisateur 192.168.0.4 a envoyé un message a l'utilisateur local et ce dernier n'a pas répondu. 

Le choix d'inclure le pseudo dans le corps du message peut paraître étonnant, mais c'est une solution efficace qui a été trouvé pour être sûr et certain de conserver l'historique des messages et l'historique des pseudo d'une même personne au cours de la conversation. Le dernier pseudo en date est conservé dans la table `Users`.
Ainsi, avec la requête présentée précédemment, il est aisé de retrouver l'historique des conversations pour chaque IP, et donc par conséquent pour chaque Utilisateur.  



### Serveur de présence
![](https://i.imgur.com/aWR7xGH.jpg)
<center><em>Diagramme d'activité du serveur </center></em>

#### Initialisation du serveur

Il n'y aucune phase d'initialisation du servlet. La méthode doGet() est définie et cela suffit. Dans cette version du programme, le serveur s'initialise tout de même en créant 20 utilisateurs (voir Installation - Premier Démarrage ).

#### Description des interactions

Le point d'entrée du serveur est http://serveurAdr:8080/clavardage/servlet
La servlet responsable du traitement des requêtes HTTP est réalisé grâce à la classe héritée `ServletServer`, cette classe délègue ensuite le traitement final à la classe `ServerManager`.
En fonction de la valeur du paramètre GET "Notif" sur ce point d'entrée, différentes actions vont être réalisés côté back-end.
Par client, on entends "l'ordinateur qui a émis la requête HTTP".
Voici la liste des interactions :

<ul>
<li><code>Notif=fetchDatabase</code>: Notification la plus commune. Le serveur renvoie  au client  la liste des utilisateurs en données JSON.</li>
<li><code>Notif=humanReadable</code>: Même effet que pour la précédente notification, sauf que la liste est renvoyé avec du formatage HTML pour permettre une lecture plus agréable des utilisateurs connectés. Cette requête est normalement utilisée par un humain qui vient voir combien d'utilisateurs sont connectés grâce à son navigateur, l'application ne l’exécute jamais.*</li>
<li><code>Notif=CONNECTED:(pseudo)</code>: Le serveur ajoute un utilisateur dans sa table locale avec :
<ul>
<li>Comme adresse : Adresse du client qui a exécuté cette requête.</li>
<li>Comme pseudo : Le pseudo indiqué entre parenthèses.<br>
</li>
</ul>
Une fois la table utilisateur mise à jour, elle est envoyée à tous les utilisateurs connectés.
</li>
<li><code>Notif=DISCONNECTED:(pseudo)</code>: Le serveur supprime de sa table locale l'utilisateur qui a l'adresse du client. Une fois la table utilisateur mise à jour, elle est envoyée à tous les utilisateurs connectés. </li>
<li><code>Notif=PSEUDO:(pseudo)</code> : Le serveur change le pseudo  de l'utilisateur qui a l'adresse IP du client par le pseudo entre parenthèses. </li>
</ul>


\* : N'hésitez pas à tester cette fonctionnalité en vous connectant sur le serveur depuis le navigateur, http://serveurAdr:8080/clavardage/servlet?Notif=humanReadable




# Installation

## Tomcat - Serveur de présence

Rien n'est à installer, nous avons fait le choix de livrer Tomcat avec le logiciel, cependant une petite configuration est à faire.
Premièrement il est nécessaire d’avoir installé le JDK (au minimum le JRE) et d’avoir configuré correctement ces deux variables d’environnements :

 - `JAVA_HOME` qui doit pointer vers la racine de votre JDK
 - `JRE_HOME` qui doit pointer vers la racine de votre JRE

Par exemple sous Windows :
JAVA_HOME = C:\Program Files\Java\jdk1.8.0_191
JRE_HOME = C:\Program Files\Java\jdk1.8.0_191\jre

Normalement ces deux variables sont automatiquement définies sous linux lors de l'installation de Java.
Vous pouvez le vérifier la valeur de ces variables avec la commande :
- Linux

		echo "$JAVA_HOME"
		echo "$JRE_HOME"
- Windows 

		set JAVA_HOME
		set JRE_HOME

Ces deux variables d'environnements sont nécessaires pour démarrer le serveur de présence. Le serveur HTTP Apache Tomcat s'éteint instantanément si il ne les détecte pas.

Le port d'Apache par défaut est 8080, vous pouvez le modifier dans apache-tomcat-9.0.14/conf/server.xml dans <connector>



## Base de données MySQL

La persistance est **optionnelle**, si MySQL n'est pas correctement configuré, l'application démarrera sans cette fonctionnalité !

Pour activer la persistance des messages sur le client, il est nécessaire :

- D'installer un serveur MySQL 
- De créer une BDD : `clavaradge`
- De créer deux tables dans cette BDD :
  - `Messages` : Contient les messages et l'horodatage.
  - `Users` : Contient la correspondance IP:Pseudo


Voici les requêtes SQL (sous la syntaxe MySQL) à utiliser : 

 - Création et utilisation de la base de données :

	    CREATE DATABASE clavardage;
	    USE clavardage;
	    
 - Création des tables :
 
	
		CREATE TABLE Users (
			IPDest VARCHAR(15),
			Pseudo VARCHAR(60),
			PRIMARY KEY (IPDest)
		);
	
		CREATE TABLE Messages (
			IPDest VARCHAR(15),
			Message VARCHAR(500),
			Date DATETIME,
			FOREIGN KEY (IPDest) REFERENCES Users(IPDest) 
		);

Un SGBD différent  de MySQL peut être utilisé, il faudra alors penser a changer le driver JDBC. Actuellement il est configuré pour se connecter à une base de données MySQL. Pour modifier le driver, aller dans le fichier SQLDB.java, il est au début du code :

	JDBC_DRIVER = "com.mysql.cj.jdbc.Driver"
	
Une solution intégrée à la JVM comme Apache Derby ou SQLite aurait aussi pu être utilisé. Nous nous plaçons dans le paradigme de l'entreprise, donc il est envisageable que les besoins pour une base de données soient partagés avec d'autre logiciel. Notre choix final fût donc une BDD installée et configurée sur la machine et non-portative comme les solutions mentionnées plus-haut. 

**NB** : Souvenez-vous de vos identifiants MySQL, ils seront demandés à l'ouverture de l'application. 

## Libraires externes requises


Ces librairies sont utilisées par javac pour compiler les sources, elles sont présentes dans le dossier /lib, vous n'avez donc pas besoin de les télécharger. 

- `Gson-2.8.5` : Conçu par Google, permet de *serializer* ou *deserializer* un objet en données JSON.
- `Servlet-api` : Librairie de l'extension JEE, permet de manipuler des servlet et donc de gérer des requêtes HTTP côté serveur. (Non utilisé côté client)
- `mysql-connector-java-8.0.13` : Permet à Java 8  de se *connecter* a une base de donnée MySQL.



# Quick start

Une fois ces variables définies, il est possible d'utiliser les scripts .bat ( Batch pour Windows ) et .sh ( Bash pour Linux ) pour exécuter ce que l'on souhaite. Ces scripts se situent à la racine du projet, et voici leur utilités :

- `start_server_clavarage` Démarre simplement le serveur de présence Tomcat en appelant startup dans apache/bin/.
La webapp correspondant à l'application de clavardage a déjà été pré-configuré. Vous la trouverez dans apache/webapps/clavardage
- `stop_server_clavardage` Shutdown le serveur de présence en appelant shutdown dans apache/bin
- `compile_and_start_client` Compile les sources et exécute le type de client que vous souhaitez, GUI ou CLI.

### Client

Le script pour démarrer le client compile les sources, pour cette raison vous devez avoir le JDK d'installé. 
**NB :** Au cas où les scripts ne fonctionneraient pas, un jar executable du client  en CLI et en GUI est fourni à la dernière version release. 
 Normalement les identifiants pour la BDD SQL sont hard-codés, mais pour que vous puissiez tester la persistance, ils seront demandés à l’exécution du jar. Il n'y aura pas de test de connexion a la BDD, si les identifiants sont erronées, la persistance sera désactivée.
Dans tout les cas, notez bien l'adresse du serveur, elle vous sera demandée lors de l’exécution du client. 

### Serveur

Aucun jar n'est fourni pour le serveur web. Normalement, il suffit simplement de définir les variables d'environnements pour que le serveur se lance sans problèmes grâce au script `start_server_clavardage`.
Si malgré tout il existe des problèmes, vous devrez setup le serveur manuellement. Vous trouverez la webapps clavardage intégrée à Apache dans apache-tomcat-9.0.14\webapps\clavardage. Il suffit de copier/coller cette webapps dans votre apache fonctionnel et de le démarrer pour avoir accès à l'application.

À des fins de **tests uniquement** pour remplir artificiellement la table des utilisateurs, 20 utilisateurs sont créés au lancement du serveur :

	User2:127.0.0.2
	     ⋮
	User20:127.0.0.20

Depuis la machine serveur, vous devez les voir si vous vous connectez avec le navigateur sur :
http://localhost:8080/clavardage/servlet?Notif=humanReadable
Vous avez également à disposition la version utilisée par l'application, qui liste directement les objets User sérialisés en JSON pour traiter les données plus facilement :
http://localhost:8080/clavardage/servlet?Notif=fetchDatabase

Préférez la version user-friendly avec la première URL. Ces URL sont aussi accessibles depuis une autre machine, il suffit de remplacer localhost par l'IP du serveur. 
De par le fonctionnement de l'application (*cf : Architecture*), ils finiront par disparaître si vous lancez un client.  Il est **vain** d'essayer de se connecter et d'envoyer un message à ces utilisateurs, il n'y a personne au bout du fil. Cela aura pour effet de bloquer l'application, car la connexion TCP initiée par le client n'est pas "threadée", donc la GUI va appeler la méthode *connect()* sur un socket, et va être bloquée jusqu’à ce que le timeout du 3-WAY Handshake TCP soit écoulé. 


# Problèmes actuels

Une seule fonctionnalité pose problème, c'est l'envoie de fichier cross-platform. En effet, lorsqu'un client sous Windows envoie un fichier à un autre client qui lui est sous Linux, le fichier va s'écrire **correctement** dans le dossier prévu à cet effet, mais le nom sera erroné. 
Cela est dû à la méthode de récupération du nom du fichier entrant. On utilise un objet Path pour récupérer le nom du fichier, cependant à cause du formatage différent des chemins de dossier (\ pour Windows et / pour Linux ), un problème survient. Cette méthode est utilisée ligne 41 du fichier `ThreadListenerSonTCP.java`.

