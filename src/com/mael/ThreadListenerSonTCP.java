package com.mael;

import java.io.*;
import java.net.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ThreadListenerSonTCP extends Thread {
	
	private Socket socket;
	private Message mes;
	private Scanner input;
	/**
	 * Constructeur par défaut (thread fils)
	 * @see ThreadListener
	 * @param socketClient
	 */
	public ThreadListenerSonTCP(Socket socketClient, Scanner input) {
		this.socket = socketClient;
		this.input = input;
	}
	
	public void run() {
		//System.out.println("[ThreadListenerSon] Connexion avec le client : " + this.socket.getInetAddress());
		
		ObjectInputStream in;
		//BufferedReader in; //Creates a buffering character-input stream that uses a default-sized input buffer.
		try {
			//in = new BufferedReader(new InputStreamReader(socket.getInputStream())); //getInputStream : Returns an input stream for this socket.
			in = new ObjectInputStream(socket.getInputStream());
			
			//Deserialization
			mes = (Message) in.readObject();

			if(mes.file==true){


				String taille = humanReadableByteCount(mes.bytes.length,true);
				System.out.println((char)27 + "[31mFichier recu : "  + (char)27 + "[0m" + mes.text + " " + taille);
				Path p = Paths.get(mes.text);
				String name = p.getFileName().toString();

				//Creation d'un dossier filesReceived a l'endroit ou l'executable est lancé
				//Tout les fichiers recus seront stocké ici
				String path = "filesReceived/"+name;
				File file = new File(path);
				file.getParentFile().mkdirs();

				System.out.println((char)27 + "[31mEcriture vers : " + (char)27 + "[0m" + file.getAbsolutePath());
				OutputStream outToFile = new FileOutputStream(path);
				outToFile.write(mes.bytes);
				outToFile.close();
			}

			if(mes.file==false) {
				//Message texte recu
				if (mes.text != null) {

					//Ajout du message dans la session
					User userDistant = UserDatabase.getByAdr(this.socket.getInetAddress());
					if(userDistant == null) System.out.println("###Message recu d'un utilisateur qui n'est pas dans la DB###");
					Message temp = new Message(userDistant.pseudo + " : " + mes.text);
					SessionManager.createSession(userDistant);
					SessionManager.getSessionByAdr(userDistant.adr).historiqueMessage.add(temp);

					if(SQLDB.DBSQLEnabled) {
						SQLDB.addMessage(userDistant.adr.getHostAddress(), new Message(mes.text));
					}

					//Coloration du texte grâce aux ANSI Escape characters
					// "[ThreadListenerSon] Nouveau message ! " apparait en rouge
					//Pseudo en bleu
					System.out.print((char)27 + "[31m[ThreadListenerSon] Nouveau message ! " + (char)27 + "[0m");
					System.out.println((char)27 + "[34m" + userDistant.pseudo + (char)27 + "[0m" + " : " + mes.text);
				}

				//Database recue
				if (mes.userList != null) {
					UserDatabase.updateDatabase(mes.userList);
					System.out.println("[ThreadListenerSon] Le serveur " + this.socket.getInetAddress() + " envoie la DB : \n" + UserDatabase.printlistUser());
					if(testGUI.GUIEnabled) {
						testGUI.updateUser();
						testGUI.chatFrame.setVisible(true);
					}
				}
			}
			this.socket.close();
			MainClient.printCurseur();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
			
		}
	}


	/**
	 * formats the bytes to a human readable format
	 *
	 * @param si true if each kilo==1000, false if kilo==1024
	 */
	public static String humanReadableByteCount(final long bytes,final boolean si)
	{
		final int unit=si ? 1000 : 1024;
		if(bytes<unit)
			return bytes+" B";
		double result=bytes;
		final String unitsToUse=(si ? "k" : "K")+"MGTPE";
		int i=0;
		final int unitsCount=unitsToUse.length();
		while(true)
		{
			result/=unit;
			if(result<unit)
				break;
			// check if we can go further:
			if(i==unitsCount-1)
				break;
			++i;
		}
		final StringBuilder sb=new StringBuilder(9);
		sb.append(String.format("%.1f ",result));
		sb.append(unitsToUse.charAt(i));
		if(si)
			sb.append('B');
		else sb.append('i').append('B');
		final String resultStr=sb.toString();
		return resultStr;
	}
}
