package com.mael;

import java.net.*;
import java.util.Scanner;

/**
 * Créer un thread (père) en attente de connexion, lors d'une connexion il crée un thread fils ThreadListernerSonTCP
 *
 */
public class ThreadListener extends Thread {

	ServerSocket socketListener;
	final static int port = 5001; //private final static -> create this variable only once. private final -> create this variable for every object. and final -> constante 

	private Scanner input;

	/**
	 * Constructeur par défaut (thread père)
	 */
	public ThreadListener(Scanner input) {
		this.input = input;
		System.out.println("[ThreadListener] Lancement du Thread TCP de gestion de connexion port : " + ThreadListener.port);
	}


	public void run() {
		try {
			socketListener = new ServerSocket(port);
//			System.out.println("[ThreadListener] Attente de connexion TCP port : " + ThreadListener.port);
			while (true) {
				Socket socketClient = socketListener.accept(); // Création d'un socket client lors d'une connexion
				ThreadListenerSonTCP threadListenerSon = new ThreadListenerSonTCP(socketClient, input); //Création d'un thread pour la communication avec ce socket
				threadListenerSon.start(); //Démarrage de la connexion
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeSocketListener() {
		try {
			socketListener.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}