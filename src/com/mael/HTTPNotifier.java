package com.mael;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public abstract class HTTPNotifier {
	static String serveurAdr;

	private HTTPNotifier(){};

	static void fetch() throws IOException {
		
		URL url = new URL("http://"+serveurAdr+"/clavardage/servlet?Notif=fetchDatabase");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String reponse=in.readLine();
        
        //TypeToken est utilis� pour recuperer le type de classe d'une List<User>
        Type listType = new TypeToken<ArrayList<User>>(){}.getType();
        List<User> userList = new Gson().fromJson(reponse, listType);
        UserDatabase.updateDatabase(userList);


		in.close();
	}
	
	static void connected(String pseudo) throws IOException {

		//Si il y a un espace, on le remplace par le caractere + qui corresponds a l'espace encodé HTML
		pseudo = pseudo.replace(" ","+" );
		URL url = new URL("http://"+serveurAdr+"/clavardage/servlet?Notif=CONNECTED:("+pseudo+")");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String reponse=in.readLine();     
		in.close();

		if(testGUI.GUIEnabled) {
			testGUI.updateUser();
			testGUI.chatFrame.setVisible(true);
		}
	}
	
	static void disconnected(String pseudo) throws IOException {

		pseudo = pseudo.replace(" ","+" );
		URL url = new URL("http://"+serveurAdr+"/clavardage/servlet?Notif=DISCONNECTED:("+pseudo+")");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String reponse=in.readLine();     
		in.close();
	}
	
	static void changedPseudo(String pseudo) throws IOException {

		pseudo = pseudo.replace(" ","+" );
		URL url = new URL("http://"+serveurAdr+"/clavardage/servlet?Notif=PSEUDO:("+pseudo+")");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String reponse=in.readLine();     
		in.close();


	}
	
	

}
