package com.mael;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;

class MainClient {

	static SQLDB DBCon;
	static final Scanner sc = new Scanner(System.in);
	static User userLocal;
	//final static String serveurAdr = "81.64.83.188";
	//final static String serveurPort = "7896";


	/**
	 * M�thode qui retourne l'adresse IP de la machine de fa�on sure, car getLocalHost et getLoopBack peuvent retourner 127.0.0.1 ou une autre interface
	 * La m�thode connect sur un socket UDP set les addresses ip destination/local ( ca fait d'autre choses aussi, qui ne nous interessent pas ici )
	 */
	 public static InetAddress getLanIP(){
		InetAddress ip=null;
		try (final DatagramSocket socket = new DatagramSocket()) {
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
			ip = socket.getLocalAddress();
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		}

		return ip;

	}

	/**
	 * Affiche l'aide utilisateur
	 */
	public static void printHelp(){
		System.out.println("db : Affichage la liste des utilisateurs actifs \n" +
				"send : Passe en mode message avec un utilisateur \n" +
				"quit : Quitte l'application et vous d�connecte \n" +
				"help : Affichage la liste des commandes disponibles \n" +
				"cp : Changer de pseudo\n" +
				"sessions : afficher les sessions actives");

	}

	/**
	 * Ajoute un handler pour g�rer le CTRL+C, on envoie "DECONNECTION:(pseudo)"
	 */
	private static void catchCTRLC() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					Thread.sleep(200);
					System.out.println("Sending deconnexion notification ...");
					userLocal.deconnect();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Gere l'interaction avec l'utilisateur pour envoyer un message
	 */
	public static int chooseMessage(){

		String msg;
		String choix;

		System.out.println(UserDatabase.printPseudos());
		System.out.print("A qui voulez vous �crire ? : ");
		synchronized (sc) {
			choix = sc.nextLine();
		}
		if(UserDatabase.pseudoExist(choix) == false){
			System.out.println("L'utilisateur n'existe pas !");
			return -1;
		}
		String temp1;
		System.out.print("Texte ou fichier (t/f) ? ");
		synchronized (sc) {
			temp1 = sc.nextLine();
		}

		if(temp1.equals("t")) {
			System.out.print("Ecrivez votre message, et appuyez sur entr�e." +
					"\n/quit pour quitter" +
					"\n/history pour afficher l'historique. \n");
			boolean stop = false;
			while(stop == false) {
				synchronized (sc) {
					msg = sc.nextLine();
				}
				if(msg.equals("/history")){
					System.out.println(SessionManager.getSessionByPseudo(choix).showMessage());
				}
				else if(msg.equals("/quit")) {
					stop = true;
					break;
				} else {
					Message mestemp = new Message(msg);
					userLocal.sendMessage(mestemp, UserDatabase.getByPseudo(choix));
					if (SQLDB.DBSQLEnabled) {
						SQLDB.addMessage(UserDatabase.getByPseudo(choix).adr.getHostAddress(), mestemp);
					}
				}
				System.out.print((char)27 + "[31m>" + (char)27 + "[0m");
			}
			return 0;
		} else if (temp1.equals("f")) {
			System.out.print("Indiquez le chemin vers le fichier : ");
			synchronized (sc) {
				msg = sc.nextLine();
			}
			Message temp = new Message(msg);
			temp.file=true; //On set a true le boolean de l'objet message, pour qu'il soit correctement trait� par userLocal.chooseMessage

			userLocal.sendMessage(temp, UserDatabase.getByPseudo(choix));
			return 0;
		} else {
			printHelp();
			return -1;
		}

	}

	/**
	 * Gere l'interaction avec l'utilisateur pour choisir une session
	 */
	private static int chooseSession() {
		String choix;
		if(SQLDB.DBSQLEnabled) SessionManager.fetchFromDBSQL();
		String sessions = SessionManager.printSessions();
		if(sessions.length() == 0 ) {
			System.out.println("Aucune session");
			return -1;
		}
		System.out.println("Sessions  : " + sessions);
		System.out.print("Quel conversation voulez vous afficher ? : ");
		synchronized (sc) {
			choix = sc.nextLine();
		}
		Session ses = SessionManager.getSessionByPseudo(choix);
		if(ses == null){
			System.out.println("La session n'existe pas !");
			return -1;
		}
		System.out.print(ses.showMessage());
		return 0;

	}

	/**
	 * Affiche un curseur ">" rouge
	 */
	public static void printCurseur(){
		System.out.print((char)27 + "[31m>" + (char)27 + "[0m");
	}

	/**
	 * Fonction qui affiche toutes les IP Locales obtenues grace a InteAddress
	 * Juste utilisé pour comparer le résultat avec getLanIP en cas de besoin..
	 */
	private static void printLocalIP(){
		//Affichage des addresses IP locales
		InetAddress lanIP = getLanIP();
		InetAddress localAdr = null;
		InetAddress localAdrBis = null;
		try {
			localAdr = InetAddress.getLocalHost();
			localAdrBis = InetAddress.getLoopbackAddress();
		} catch (UnknownHostException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		System.out.println("getLocalHost.getHostAddress() : " + localAdr.getHostAddress());
		System.out.println("getLoopbackAddress.getHostAddress() : " + localAdrBis.getHostAddress());
		System.out.println("getLanIP : " + lanIP + "\n");


	}

	public static void main(String[] args) throws InterruptedException {


		System.out.println("Entrez vos identifiants pour vous connecter a la BDD SQL");
		System.out.print("Username : ");
		SQLDB.USER = sc.nextLine();
		System.out.print("Password : ");
		SQLDB.PASS = sc.nextLine();




		testGUI.GUIEnabled = false;

		//####### Connection a la BDD SQL locale pour recuperer les messages######
		SQLDB.init();
		if(SQLDB.DBSQLEnabled) System.out.println("DB SQL OK !");

		if(SQLDB.DBSQLEnabled == false) System.out.println((char)27 + "[31mLa BDD SQL semble inaccessible, mode sans persistence activ�" + (char)27 + "[0m");

		if(SQLDB.DBSQLEnabled) SessionManager.fetchFromDBSQL();



		InetAddress lanIP = getLanIP();

		//Thread listen TCP
		ThreadListener Tlistener = new ThreadListener(sc);
		Tlistener.start();



		String serveurAdr;
		boolean serverOK = false;
		while(serverOK == false){
			try {
				System.out.print("Indiquez serveurAdr:port (comme par ex localhost:8080) : ");
				serveurAdr = sc.nextLine();
				HTTPNotifier.serveurAdr = serveurAdr;
				HTTPNotifier.fetch();
				serverOK = true;
			} catch (IOException e) {
				serverOK = false;
				System.out.println("Server unreachable !");
			}
		}

		try {
			System.out.println("Recuperation des utilisateurs connectes....");
			HTTPNotifier.fetch();
		} catch(IOException e) {
			e.printStackTrace();
			System.out.println("Error while fetching database to the HTTP Server !"
					+ "V�rifier la connectivit� avec le serveur");
			System.exit(0);
		}


		//On pause le main pendant 1 sec le temps de fetch la DB
		//Pass� ce d�lai, on considère qu'il n'y a personne sur le r�seau, on construit sois-même la DB locale alors.
		Thread.sleep(1000);

		//Demande de choix de pseudo, et v�rification de l'entr�e utilisateur (alphanumerique avec des espaces possibles)
		System.out.println(UserDatabase.printlistUser());
		System.out.print("Bonjour, entrez un pseudo : ");
		String pseudoChosen = "";
		boolean stop = false;
		while(stop == false){
			synchronized (sc) {
				pseudoChosen = sc.nextLine();
			}
			if(pseudoChosen.length() > 20) System.out.println("Moins de 60 caracte	res SVP");
			else if(pseudoChosen.matches("[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*") == false) { //Petite regex de derrieres les fagots
				System.out.println("Veuillez entrer un pseudo valide ! (Lettre latines, chiffres et sans espaces à la fin)");
			} else if (UserDatabase.pseudoExist(pseudoChosen) == true ) System.out.println("Pseudo d�ja pris, veuillez en choisir un autre ");
			else stop = true;
		}

		userLocal = new User(pseudoChosen, lanIP); //User1 est l'utilisateur local
		pseudoChosen = userLocal.pseudo;

		if(SQLDB.DBSQLEnabled) SQLDB.addUser(userLocal.adr,userLocal.pseudo);
		//Signalement de notre pr�sence sur le r�seau et de notre pseudo
		try {
			HTTPNotifier.connected(pseudoChosen);
		} catch (IOException e) {
			System.out.println("Error while sending connected notification!");
			e.printStackTrace();
		}
		
		catchCTRLC();

		//Affichage de la DB locale
		System.out.println(UserDatabase.printlistUser());







		String choix;
		String msg;
		System.out.println("Tapez HELP pour connaître la liste des commandes disponibles");
		boolean quit = false;
		while(quit == false) { //Tourne en rond en attente d'un message
			synchronized (sc) {
				choix = sc.nextLine();
			}
			switch(choix) {
				case "db":
					
				try {
					HTTPNotifier.fetch();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					System.out.println(UserDatabase.printlistUser());
					break;
				case "send" :
					chooseMessage();
					break;
				case "cp":
					System.out.println("/annul pour annuler le changement de pseudo");
					System.out.print("Ecrivez votre nouveau pseudo  : ");
					synchronized (sc) {
						stop = false;
						while(stop == false){
							choix = sc.nextLine();
							if(choix.equals("/annul")) break;
							else if(choix.length() > 20) System.out.println("Moins de 60 caracteres SVP");
							else if(choix.matches("(([a-zA-Z0-9_-])+ ?){1,40}[a-zA-Z0-9]") == false) { //Petite regex de derrieres les fagots
								System.out.println("Veuillez entrer un pseudo valide ! (Lettre latines, chiffres et sans espaces à la fin)");
							} else if (UserDatabase.pseudoExist(choix) == true ) System.out.println("Pseudo d�ja pris, veuillez en choisir un autre ");
							else stop = true;
						}
					}
					if(!choix.equals("/annul")) userLocal.changePseudo(choix);
					break;
				case "sessions":
					chooseSession();
					break;
				case "quit" :
					userLocal.deconnect();
					quit=true;
					break;
				default:
					printHelp();
					break;
			}
			printCurseur();
		}


		System.out.println("Fermeture de l'application");
		System.exit(0);


	}




}
