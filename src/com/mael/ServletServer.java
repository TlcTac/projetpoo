package com.mael;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import javax.servlet.*;
import javax.servlet.http.*;


public class ServletServer extends HttpServlet {
	 
	private static final long serialVersionUID = 3976233673421565649L;
	
	private String message;

	   public void init() throws ServletException {
	      // Do required initialization
	      message = "Hello World";
	      try {
   
			
			for(int i = 2; i<40 ; i++) {
				InetAddress adr = InetAddress.getByName("100.0.0." + i);
				User user = new User("User"+i, adr);
				UserDatabase.addUser(user);
			}
			      
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	   }

	   public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException {
	      
		  Enumeration<String> test = request.getParameterNames();
		  
		  String notif = request.getParameter("Notif");
		  String ipClient = request.getRemoteAddr();
		  
		  ServerManager manager = new ServerManager(notif, ipClient,response);
		  manager.start();
		  
	      // Set response content type (MIME)

//	      // Actual logic goes here.
//		      response.setContentType("text/html");
//		      response.setCharacterEncoding("utf-8");
//		      response.addHeader("test", "46");
//	          out.println("<h1>" + "Client (GET Method) " + ipClient + " envoie " + notif  + "</h1>");

		  
		  try {
			manager.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void destroy() {
	      // do nothing.
	   }
	}
