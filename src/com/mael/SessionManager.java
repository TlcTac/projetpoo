package com.mael;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class SessionManager {

	/**
	 * Liste des sessions de l'utilisateur local
	 */
	public static  List<Session> ListSession = new ArrayList<Session>();

	
	
	/**
	 * Vérification qu'une session existe
	 * @return true si la session existe, false sinon
	 */
	public static boolean sessionExist(User userServer) {
		for (Session session : ListSession) {
			if(session.getUserServer().adr.equals(userServer.adr)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Retourne une session parmis celle enregistrée grâce au pseudo du destinataire
	 *  @return Session retourne la session
	 *  @param pseudo Un pseudo
	 */
	public static Session getSessionByPseudo(String pseudo) {
		for(Session session : ListSession) {
			if(session.userServer.pseudo.equals(pseudo)) return session;
		}
		return null;
	}

	/**
	 * Retourne une session parmis celle enregistrée grâce a l'addresse IP du destinataire
	 *  @return Session retourne la session
	 *  @param adr Une adresse InetAddress
	 */
	public static Session getSessionByAdr(InetAddress adr) {
		for(Session session : ListSession) {
			if(session.userServer.adr.equals(adr)) return session;
		}
		return null;
	}
	/**
	 * Retourne la liste des conversations actives ( ou passées si connexion a la DB )
	 *  @return String Retourne un string contenant la liste des sessions
	 */
	public static String printSessions() {
		String conversation="";
		for (Session session : ListSession) {
			conversation += session.userServer.pseudo + ", ";
		}
		return conversation;
	}

	public static void fetchFromDBSQL() {
			//Recuperation des anciennes sessions de chat depuis la DB SQL
		if(SQLDB.getUsers() == null) System.out.println("nullllll");
			List<User> users = SQLDB.getUsers();

			List<Message> messageList;
			for (User user : users) {
				Session ses = new Session(user);
				SessionManager.createSession(ses); //Ajout de la session dans la liste des sessions locale
				messageList = SQLDB.getMessages(user.adr.getHostAddress());
				//Ajout des anciens messages dans la session
				for (Message mes : messageList) {
					ses.historiqueMessage.add(mes);
				}
			}


	}


	/**
	 * Création de la session pour les deux utilisateurs après avoir vérifié qu'elle n'existe pas
	 *  @param userServer la personne avec qui l'on souhaite créer la session
	 */
	public static int createSession(User userServer) {
		if(SessionManager.sessionExist(userServer) == false) { //Vérification que la session n'existe pas
			Session newSession = new Session(userServer); //Création de la session
			ListSession.add(newSession); //Ajout de la session dans la list des sessions de l'utilisateurs
			return newSession.getIdSession();
		} else if(!SessionManager.getSessionByAdr(userServer.adr).userServer.pseudo.equals(userServer.pseudo)){
			SessionManager.getSessionByAdr(userServer.adr).userServer.pseudo = userServer.pseudo;
		}
		return -1;
	}


	public static int createSession(Session ses) {
		if(SessionManager.sessionExist(ses.userServer) == false) {
			SessionManager.ListSession.add(ses);
			return ses.getIdSession();
		} else if(!ses.userServer.pseudo.equals(SessionManager.getSessionByAdr(ses.userServer.adr).userServer.pseudo)){
			SessionManager.getSessionByAdr(ses.userServer.adr).userServer.pseudo = ses.userServer.pseudo;
		}
			return -1;

	}

}
