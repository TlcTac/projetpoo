package com.mael;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Message implements Serializable {
	
	static final long serialVersionUID=1L;

    Timestamp timestamp ;
    public String text;
    public List<User> userList; // = new ArrayList<User>();
    
    //true si c'est un fichier, faux par défaut
    boolean file = false;
    byte[] bytes;

    //Constructeur utilisé pour envoyer un message texte
    public Message(String text) {
    	this.userList = null;
    	this.text = text;
        this.timestamp =  new Timestamp(System.currentTimeMillis());
    }

    //Constructeur utilisé pour envoyer la DB locale des utilisateurs
    public Message(List<User> stack ) {
        this.text=null;
    	this.userList = stack;
        this.timestamp =  new Timestamp(System.currentTimeMillis());
    }
    

}