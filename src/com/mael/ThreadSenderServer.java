package com.mael;

import java.net.*;
import java.util.List;


import java.io.*;

public class ThreadSenderServer extends Thread {

    private Message mes;
    private InetAddress receiver;
    private int idSessionReceiver;
    PrintWriter out;
    Socket socketDest;



    public ThreadSenderServer(Message mes, InetAddress receiver) {
        this.mes = mes;
        this.receiver = receiver;
    }



    public void run() {
        


            final Socket clientSocket;
            if (receiver != null) {


                try {


                    clientSocket = new Socket(receiver, 5001);

                    if (mes.file == false) {

                        //flux pour envoyer un objet
                        ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());

                        out.writeObject(mes);
                        out.flush();
                        if (mes.text != null) System.out.println("[ThreadSender] Envoi du message : " + mes.text);


                    }

                    if (mes.file == true) {

                        //Stream vers le destinataire
                        ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());



                        File file = new File(mes.text);
                        InputStream in = null;

                        try { in = new FileInputStream(file);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }


                        // Get the size of the file
                        //TODO
                        //Si la taille du fichier est trop grande (5Mo ? ) ne pas envoyer le fichier
                        //Test effectue pour du .txt, .png pour 20octets a 5Mo sans probleme
                        int length = (int) file.length();
                        System.out.println(length);

                        //Remplir le tableau de bytes du message de la taille adequate
                        mes.bytes = new byte[16 * length];

                        //in.read retourne le nombre d'octets lu et place tout les octets lu depuis le stream in dans mes.bytes
                        //tant qu'il y a encore de la lecture on attends
                        while (( in .read(mes.bytes)) > 0) {}
                        out.writeObject(mes);
                        out.flush();
                        System.out.println("[ThreadSender] Envoi du fichier : " + mes.text + "taille " + mes.bytes.length);


                    }

                } catch (java.net.ConnectException e) {
                    System.out.println("Connexion avec l'adresse " + receiver + " impossible, l'user est peut etre deconnecte");
                    System.out.println("Suppression de l'user dans la DB serveur...");
                    UserDatabase.userList.remove(UserDatabase.getByAdr(receiver));
                } catch (IOException e) {
                    System.out.println("Connexion avec l'adresse " + receiver + " impossible, l'user est peut etre deconnecte");
                    System.out.println("Suppression de l'user dans la DB serveur...");
                    UserDatabase.userList.remove(UserDatabase.getByAdr(receiver));
                }
            }
            else System.out.println("[ThreadSender] Error : Receiver null");

        
    }
}