package com.mael;

import java.io.*;
import java.net.*;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;


public class ServerManager extends Thread{
	
    DatagramSocket serverSocket;  
    byte[] receiveData = new byte[1024];
    byte[] sendData = new byte[1024];
    private String notif;
    InetAddress IPAddress;
    HttpServletResponse response;
    
	public ServerManager(String notif, String IPAddress,HttpServletResponse response) {
		try {
			this.IPAddress = InetAddress.getByName(IPAddress);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.response = response;
		this.notif = notif;
	}
	
	public void run() {

				
				System.out.println("Servlet received : " + notif);
				String reponse = null;
				if(notif != null) {
					//Si le message commence par un string "PSEUDO:", alors c'est un changement de pseudo
					if (notif.substring(0, 7).equals("PSEUDO:")) {
						//Le client signale en UDP qu'il a change de pseudo
						int startInd = notif.indexOf("(");
						int endInd = notif.indexOf(")");
						reponse = notif.substring(startInd + 1, endInd);


						//Mise a jour de la table
						for (Iterator<User> it = UserDatabase.userList.iterator(); it.hasNext(); ) { 
							User temp = it.next();
							if (temp.adr.equals(IPAddress)) {
								System.out.println("Changement de pseudo, " + temp.pseudo + " devient " + reponse);
								temp.pseudo = reponse;
							}
						}
						
						//Reponse HTTP
						PrintWriter out;
						try {
							out = response.getWriter();
							out.println("OKCP");
							out.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
                        //Envoi a tous les utilisateurs l'utilisateur deconnecte
                        for (User client : UserDatabase.userList) {
                            System.out.println("Envoi de la nouvel base de donnee a l'utilisateur :" + client.pseudo);
                            Message db = new Message(UserDatabase.userList);
                            ThreadSenderServer t = new ThreadSenderServer(db, client.adr);
                            t.start();
                            
                        }
						

					}
					
					


					//Si le message commence par un string "CONNECTED:", alors c'est une deconnexion
					else if (notif.substring(0, 10).equals("CONNECTED:")) {

						//On isole le pseudo, le message recu est de cette forme CONNECTED:(pseudo)
						int startInd = notif.indexOf("(");
						int endInd = notif.indexOf(")");
						reponse = notif.substring(startInd + 1, endInd);

						User temp = new User(reponse, IPAddress);
								System.out.println("L'utilisteur  " + reponse + " vient de se connecter");
								UserDatabase.addUser(temp);

								
						//Reponse HTTP
						PrintWriter out;
						try {
							out = response.getWriter();
							out.println("OKC");
							out.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
                        //Envoi a tous les utilisateurs l'utilisateur deconnecte
                        for (User client : UserDatabase.userList) {
                            System.out.println("Envoi de la nouvel base de donnee a l'utilisateur :" + client.pseudo);
                            Message db = new Message(UserDatabase.userList);
                            ThreadSenderServer t = new ThreadSenderServer(db, client.adr);
                            t.start();
                        }


					}

					//Si le message commence par un string "DISCONNECTED:", alors c'est une deconnexion
					else if (notif.substring(0, 13).equals("DISCONNECTED:")) {

						//On isole le pseudo, le message recu est de cette forme DISCONNECTED:(pseudo)
						int startInd = notif.indexOf("(");
						int endInd = notif.indexOf(")");
						reponse = notif.substring(startInd + 1, endInd);

						User temp = UserDatabase.getByPseudo(reponse);
						System.out.println("L'utilisteur  " + reponse + " vient de se deconnecter");
						UserDatabase.userList.remove(temp);
						
						
						//Reponse HTTP
						PrintWriter out;
						try {
							out = response.getWriter();
							out.println("OKDC");
							out.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
                        //Envoi a tous les utilisateurs l'utilisateur deconnecte
                        for (User client : UserDatabase.userList) {
                            System.out.println("Envoi de la nouvel base de donnee a l'utilisateur :" + client.pseudo);
                            Message db = new Message(UserDatabase.userList);
                            ThreadSenderServer t = new ThreadSenderServer(db, client.adr);
                            t.start();
                        }


					}

					//Demande de database
					else if (notif.substring(0, 13).equals("humanReadable")) {
							
						
					      response.setContentType("text/html");
					      response.setCharacterEncoding("utf-8");
				    	  String db = new Gson().toJson(UserDatabase.userList);
				    	  PrintWriter out;
				    	  
						try {
							out = response.getWriter();
							out.println("<link rel='stylesheet' type='text/css' href='css/userList.css'>"
									 + "<div>\r\n" + 
									"  <h2>Utilisateurs connectes</h2>\r\n" + 
									"  <ul>\r\n");
							for(User user : UserDatabase.userList)
								out.println("<li><a href='#'>"+user.pseudo+" : " +  user.adr.getHostAddress() + "</a></li>\r\n");
							out.println("  </ul>\r\n" + 
									"</div>");
							out.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	  
					}
					else if (notif.substring(0, 13).equals("fetchDatabase")) {
						
						  response.setContentType("application/json");
				    	  String db = new Gson().toJson(UserDatabase.userList);
				    	  PrintWriter out;
				    	  
						try {
							out = response.getWriter();
							out.println(db);
							out.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	  
					}
					
				}

		System.out.println("Fermeture du thread");
	}
	

}
