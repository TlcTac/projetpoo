package com.mael;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.awt.*;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;


public class testGUI extends JFrame {

    //List des sessions graphiques, a voir si on est obligé de la faire...
    public static List<JTextArea> sessionList = new ArrayList<JTextArea>();
    public static List<JScrollPane> sessionListScroll = new ArrayList<JScrollPane>();
    public static JTextArea envoieMessage;
    public static JButton envoyer;
    public static JButton envoyerFile;
    public static JPanel session;
    public static String pseudo;
    public static JPanel userList;
    public static JFrame chatFrame;
    public static JScrollPane scrollEnvoie;
    public static JTextArea DB;


    static boolean GUIEnabled = true;

    public static User userLocal;

    static boolean serverpassed =false;



    static String serveurAdr = "localhost";
    final static String serveurPort = "8080";

    public testGUI(){

        initUI();
    }

    public void initUI() {

        chatFrame = new JFrame();
        //Les setVisible(true) sont autant utilisés  pour rendre visible que pour refresh la fenetre ( voir validate() )

        //####### Connection a la BDD SQL locale pour recuperer les messages######
        SQLDB.init();
        if(SQLDB.DBSQLEnabled) System.out.println("DB SQL OK !");

        if(SQLDB.DBSQLEnabled == false) System.out.println((char)27 + "[31mLa BDD SQL semble inaccessible, mode sans persistence activ�" + (char)27 + "[0m");

        if(SQLDB.DBSQLEnabled) SessionManager.fetchFromDBSQL();

        //Parametrage de l'écran de connexion
        setTitle("Ecran de connexion");
        setSize(400, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(null);




        //              Frame de l'ecran de connexion
        JTextField tf=new JTextField();
        tf.setBounds(50,50, 150,20);
        tf.setToolTipText("Entrer pseudo");

        JLabel pseudoAvertissement = new JLabel("Choisir un pseudo :");
        pseudoAvertissement.setBounds(50,50,2000,50);

        //Lorsqu'on appui sur ce bouton, le logiciel envoie sur le réseau le pseudo choisi
        //Il démarre aussi le thread d'écoute UDP si le pseudo n'est pas déja pris
        JButton entrerChatButton = new JButton("Entrer dans le chat");
        entrerChatButton.setBounds(100,100,200,20);
        entrerChatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String test;
                test = tf.getText();
                pseudo=test;

                //TODO
                //Implémenter une méthode non-blocante pour le GUI si le pseudo est déja pris
                userLocal = new User(pseudo, MainClient.getLanIP());
                if (UserDatabase.pseudoExist(pseudo) == false ) {
                    //UserDatabase.addUser(userLocal);
                    if(SQLDB.DBSQLEnabled) SQLDB.addUser(userLocal.adr,userLocal.pseudo);

                    //Signalement de notre présence sur le réseau et de notre pseudo
                    try {

                        HTTPNotifier.connected(pseudo);
                    } catch (IOException o) {
                        System.out.println("Error while sending connected notification!");
                        o.printStackTrace();
                    }


                    setVisible(false);
                    chatFrame.setTitle("Clavaradge - Vous êtes " + pseudo);
                    chatFrame.setVisible(true);
                    updateUser();
                    ThreadFetch.start();


                } else {
                    pseudoAvertissement.setText("<html><div style=\"color:red\">Pseudo deja pris ! Veuillez en choisir un autre</div></html>");
                }

            }

        });


        //Pour entrer dans le chat si appui sur entrée
        tf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                entrerChatButton.doClick();
            }
        });


        //Label de test
        DB = new JTextArea(UserDatabase.printlistUser());
        DB.setLineWrap(true); //Pour retourner a la ligne si phrase trop grande au lieu d'agrandir le JTextArea
        DB.setWrapStyleWord(true);
        DB.setEditable(false);
        JScrollPane scrollDB = new JScrollPane (DB,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollDB.setBounds(0,200,200,200);

        //Bouton pour refresh
        JButton refresh = new JButton("Rafraichir la DB");
        refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    HTTPNotifier.fetch();
                    System.out.println(UserDatabase.printlistUser());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                DB.setText(UserDatabase.printlistUser());
                setVisible(true);
            }
        });
        refresh.setBounds(200,200,200,20);


        JLabel lb = new JLabel("");
        if(SQLDB.DBSQLEnabled) lb.setText("<html><div style=\"color:green\">DB SQL Connected !</div></html>");
        else lb.setText("<html><div style=\"color:red\"> DB SQL inaccessible. <br> Mode sans persistance activé </div></html>");
        lb.setBounds(200,250,200,100);



        add(lb);
        add(refresh);
        add(scrollDB);
        add(entrerChatButton);
        add(pseudoAvertissement);
        add(tf);
        setVisible(true);



        //            Frame de l'écran de chat


        //Parametrage de l'ecran principal du chat
        chatFrame.setTitle("CLAVARDAAAAAGE");
        chatFrame.setSize(500, 700);
        chatFrame.setLocationRelativeTo(null);
        //Le chat est séparé en deux colonnes, une a gauche pour les utilisateurs et a droite pour les emssages
        chatFrame.setLayout(new GridLayout(1,2));


        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Parametres");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription(
                "menu parametre");
        menuBar.add(menu);

        JMenuItem menuItem = new JMenuItem("Changer de pseudo");
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Vous pouvez choisir un nouveau pseudo");
        menu.add(menuItem);

        menuItem.addActionListener(new ChangePseudo());

        chatFrame.setJMenuBar(menuBar);

        //  Conversation
        session = new JPanel();
        session.setLayout(new GridLayout(4,1)); //Une ligne pour recevoir, une pour envoyer, et l'autre pour le bouton



        //  Liste utilisateur ( sous forme de bouton a cliquer sur la gauche )
        userList=new JPanel();
        userList.setBounds(0,0,200,200);
        userList.setBackground(Color.gray);
        //20 lignes et 1 colonne d'utilisateur
        //TODO
        //Ne pas mettre un nombre fixe de ligne, si il y a plus d'users ca vas bugger
        userList.setLayout(new GridLayout(20,1));




        //Test label
        JLabel list = new JLabel();
        //Utilisation d'HTML pour formater plus facilement les strings
        list.setText("<html>"+ UserDatabase.printlistUser() +"</html>");
        list.setBounds(0,0,200,300);
        userList.add(list);



        //Champ où l'on écrit pour envoyer un message, même objet pour tout les utilisateurs
        envoieMessage = new JTextArea("Saisir un message...");
//        envoieMessage.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                //Envoyer un message si appui sur entrée
//                envoyer.doClick();
//            }
//        });
        envoieMessage.setLineWrap(true); //Pour retourner a la ligne si phrase trop grande au lieu d'agrandir le JTextArea
        envoieMessage.setWrapStyleWord(true);
        scrollEnvoie = new JScrollPane (envoieMessage,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);


        //Bouton envoyer, c'est le meme objet tout le temps.
        //Par contre son actionlistener est modifié lorsqu'on clique sur un utilisateur a gauche
        //Voir les méthodes showsession et sendmessage
        envoyer = new JButton("ENVOYER");
        envoyerFile = new JButton("ENVOYER FICHIER");



        chatFrame.add(userList);
        chatFrame.add(session);

        //Confirmation de fermeture
        chatFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
        chatFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                int result = JOptionPane.showConfirmDialog(chatFrame,
                        "Do you want to Exit ?", "Exit Confirmation : ",
                        JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    User usr = UserDatabase.getByPseudo(pseudo);
                    if(usr != null) usr.deconnect();
                    chatFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
                else if (result == JOptionPane.NO_OPTION)
                    chatFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            }
        });


    }

    //Appelée lorsqu'on souhaite mettre a jour la liste des boutons a gauche.
    //Appelée a chaque notification recue par UDPListener
    //Appelée aussi lors de l'entrée dans le chat
    public static void updateUser() {

        userList.removeAll();


        for(User user : UserDatabase.userList){
            JButton userButton = new JButton(user.pseudo);
            //TODO
            //Mettre le JTextarea dans un JScrollpane pour pouvoir scroll si trop de message
            JTextArea messageRecu1 = new JTextArea("MESSAGES RECUS");
            messageRecu1.setLineWrap(true); //Pour retourner a la ligne si phrase trop grande au lieu d'agrandir le JTextArea
            messageRecu1.setWrapStyleWord(true);
            JScrollPane scrollReception = new JScrollPane (messageRecu1,
                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            sessionList.add(messageRecu1);
            sessionListScroll.add(scrollReception);
            //session.add(messageRecu1);
            userButton.addActionListener(new showSession(messageRecu1,user,scrollReception));

            userList.add(userButton);


        }

        userList.repaint();
        chatFrame.revalidate();

    }

    static class showSession implements ActionListener {
        JTextArea jt;
        JScrollPane scrollReception;
        User userDest;

        public showSession(JTextArea jt, User userDest, JScrollPane scrollReception) {
            this.jt = jt;
            this.scrollReception = scrollReception;
            this.userDest = userDest;
        }
        //On cache toute les autres conversations, et on affiche la bonne
        public void actionPerformed(ActionEvent event) {
            session.removeAll();
            for(JScrollPane oldjcp : sessionListScroll) session.remove(oldjcp);

            //on remove et on add les elements graphiques juste pour les repositionner dans l'ordre..
            //Il faudrait utiliser gridbaglayout avec des constraints pour etre largement plus propre
            session.remove(envoieMessage);
            session.remove(envoyer);
            session.add(scrollReception);
            session.add(scrollEnvoie);
            session.add(envoyer);
            session.add(envoyerFile);


            for(JScrollPane messageRecu : sessionListScroll){
                messageRecu.setVisible(false);
            }
            this.scrollReception.setVisible(true);
            if(SQLDB.DBSQLEnabled) SessionManager.fetchFromDBSQL();
            if(SessionManager.sessionExist(userDest) == true){
                System.out.println("session avec" + userDest.pseudo + " existe");
                this.jt.setText(SessionManager.getSessionByPseudo(userDest.pseudo).showMessage());
            }
            //On lie le bouton envoyer a la bonne session
            for(ActionListener al : envoyer.getActionListeners())
                envoyer.removeActionListener(al);
            for(ActionListener al : envoyerFile.getActionListeners())
                envoyerFile.removeActionListener(al);
            envoyer.addActionListener(new sendMessage(this.jt,userDest));
            envoyerFile.addActionListener(new sendFile(this.jt,userDest));

            chatFrame.setVisible(true);
        }

    }

    static class sendFile implements  ActionListener {
        JTextArea jt;
        User userDest;
        JFileChooser fc;

        public sendFile(JTextArea jt, User userDest) {
            this.jt = jt;
            this.userDest = userDest;
            fc = new JFileChooser();
        }
        public void actionPerformed(ActionEvent e) {
            int returnVal = fc.showOpenDialog(chatFrame);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
                envoieMessage.append("Opening: " + file.getName() + ".\n");
                Message temp = new Message(file.getAbsolutePath());
                temp.file=true;
                UserDatabase.getByPseudo(pseudo).sendMessage(temp, userDest);
            } else {
                envoieMessage.append("Open command cancelled by user.\n");
            }
            chatFrame.setVisible(true);
        }
    }

    static class sendMessage implements ActionListener {
        JTextArea jt;
        User userDest;

        public sendMessage(JTextArea jt, User userDest) {
            this.jt = jt;
            this.userDest = userDest;
        }
        public void actionPerformed(ActionEvent e) {
            String test = envoieMessage.getText();
            jt.append("\n" + pseudo + " : " + test);

            Message temp = new Message(test);
            UserDatabase.getByPseudo(pseudo).sendMessage(temp, userDest);
            if (SQLDB.DBSQLEnabled) {
                SQLDB.addMessage(UserDatabase.getByPseudo(pseudo).adr.getHostAddress(), temp);
            }
            //System.exit(0);
            chatFrame.setVisible(true);
        }
    }

    static class ChangePseudo implements ActionListener {

        public ChangePseudo() {

        }

        public void actionPerformed(ActionEvent e) {
            JDialog d = new JDialog(chatFrame, "dialog Box");
            d.setLayout(new GridLayout(2,1));
            JLabel label = new JLabel("Entrer le nouveau pseudo");
            d.setSize(100, 100);
            d.add(label);

            JTextField tf=new JTextField();
            tf.setToolTipText("Entrer le nouveau pseudo");
            tf.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String pseudo = tf.getText();
                    userLocal.changePseudo(pseudo);
                    label.setText("Changemetn effectué ! Vous vous appelez maintenant " + pseudo);
                    testGUI.pseudo = pseudo;
                    updateUser();
                    d.dispose();
                }

            });


            d.add(tf);
            d.setVisible(true);
        }

    }

    static Thread ThreadFetch;

    public static void main(String [] args) throws UnknownHostException {

//        User user1 = new User("Eric", InetAddress.getByName("192.168.43.111"));
//        User user2 = new User("Michel", InetAddress.getByName("192.168.43.83"));
//        User user3 = new User("Jean",InetAddress.getByName("127.0.0.1"));



        //TODO
        //Utiliser des layouts gridbaglayout plutot que gridlayout pour mettre une taille différente
        //sur tous les éléments.. Par exemple rendre tout les buttons plus petits.

        Scanner input = new Scanner(System.in);

        //Thread listen TCP
        ThreadListener Tlistener = new ThreadListener(input);
        Tlistener.start();

        ThreadFetch = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){

                    try {
                        HTTPNotifier.fetch();
                        System.out.println("Fetch OK");
                    } catch (IOException e) {
                        System.out.println("Le serveur HTTP " + HTTPNotifier.serveurAdr +  " ne reponds pas");
                    }
                    testGUI.updateUser();

                    try {
                        Thread.currentThread().sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            }
        });


        serverAsk();
        while(serverpassed==false){
            try {
                Thread.currentThread().sleep(2000);
                System.out.println("Main sleep...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }






        try {
            HTTPNotifier.serveurAdr = serveurAdr;
            HTTPNotifier.fetch();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(UserDatabase.printlistUser());
        System.out.println(MainClient.getLanIP().getHostAddress());

        //NOTE
        //UDPListener a été modifié pour ajouter updateUser
        //Toutes les notifications,messages et affichages sont présents dans la console a des fins de débuggage
        //Il faut parfois recadrer la fenetre (avec la souris) pour faire refresh des changements meme si c'est normalement réparé
        //Il faut cliquer sur un bouton a gauche pour rentrer activement dans une session, utiliser les champs a droite sans avoir séléectionner
        //auparavant un user n'a pas de sens ( et ca bug )

        EventQueue.invokeLater(() -> {
            JFrame ex = new testGUI();
            ex.setVisible(true);
        });

    }



    public static void serverAsk(){
        JFrame frame = new JFrame("Server select");
        frame.setLayout(new GridLayout(7,1));

        JLabel question = new JLabel("Indiquez serverIP:port");
        JTextArea select = new JTextArea("localhost:8080");

        JLabel question2 = new JLabel("Indiquez l'username de MySQL");
        JTextArea select2 = new JTextArea("root");

        JLabel question3 = new JLabel("Indiquez le mot de passe de MySQL");
        JTextArea select3 = new JTextArea("root");




        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                testGUI.serveurAdr = select.getText().trim();
                try {
                    HTTPNotifier.serveurAdr = serveurAdr;
                    HTTPNotifier.fetch();
                    testGUI.serverpassed = true;
                    SQLDB.USER = select2.getText().trim();
                    SQLDB.PASS = select3.getText().trim();
                    frame.setVisible(false);
                } catch (IOException a) {
                    testGUI.serverpassed = false;
                    question.setText("Server unreachable !");
                }
            }
        });
        frame.add(question);
        frame.add(select);
        frame.add(question2);
        frame.add(select2);
        frame.add(question3);
        frame.add(select3);
        frame.add(okButton);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
