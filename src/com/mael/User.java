package com.mael;

import java.util.Iterator;
import java.io.IOException;
import java.io.Serializable;
import java.net.*;
import java.util.Scanner;


class User implements Serializable {
	
	static final long serialVersionUID=3L;
	
	public InetAddress adr;
	/**
	 * Pseudo de l'utilisateur
	 */
	public  String pseudo;
	
	
	
	/**
	 * Constructeur par défaut, instanciation d'un utiisateur
	 * @param pseudo Son pseudo
	 * @param adr Son addresse InetAddress
	 */
	public User(String pseudo, InetAddress adr) {
		this.pseudo = pseudo;
		this.adr = adr;
	}
	

	
	/**
	 * Envoi un message à l'utilisateur via un thread sender, si la session n'existe pas elle est créée
	 * @param message Le message a envoyer
	 * @param receiver L'utilisateur de destination
	 * @see ThreadSender
	 */
	public void sendMessage(Message message, User receiver) {
		int idSessionReceiver;

		if(SessionManager.sessionExist(receiver) == false) { //La session n'existe pas
			System.out.println("[User] Creation d'une session avec " + receiver.pseudo);
			idSessionReceiver = SessionManager.createSession(receiver); //Créer la session
		}

		//Ajout du message dans la session
		Message temp = new Message(this.pseudo + " : " + message.text);
		SessionManager.getSessionByAdr(receiver.adr).historiqueMessage.add(temp);
		ThreadSender.send(message, receiver.adr);
		//System.out.println(SessionManager.getSessionByAdr(receiver.adr).showMessage());

	}	


	//1 si erreur, 0 si succès
	public int changePseudo(String pseudoChosen) {
		if(UserDatabase.pseudoExist(pseudoChosen) == true ) {
			System.out.println("Pseudo already taken");
			return 1;
		}


		//Mise a jour de la table
		for(User user : UserDatabase.userList) {
			if(this.adr.equals(user.adr) ) {
				user.pseudo = pseudoChosen;
			}
		}
		if(SQLDB.DBSQLEnabled) SQLDB.changePseudo(this.adr.getHostAddress(), pseudoChosen);

		//Mise a jour de l'user
		this.pseudo = pseudoChosen;
		
		
		Message mes = new Message("PSEUDO:("  + pseudoChosen +")");
		System.out.println("Nouveau pseudo : " + pseudoChosen);
		try {
			HTTPNotifier.changedPseudo(pseudoChosen);
		} catch (IOException e) {
			System.out.println("Error while sending changed pseudo notification to the HTTP Server !");
			e.printStackTrace();
		}
		return 0;
		
	

	}

	public void deconnect() {
		Message mes = new Message("DISCONNECTED:("  + this.pseudo +")");

		
		try {
			HTTPNotifier.disconnected(this.pseudo);
		} catch (IOException e) {
			System.out.println("Error while sending connected notification!");
			e.printStackTrace();
		}


		UserDatabase.userList.remove(this);
	}
}
