package com.mael;


import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//stack of user connected

//Implements serializable enoyer l'objet stack
public class UserDatabase  implements Serializable {
	
	static final long serialVersionUID=2L;
	
	static List<User> userList = new ArrayList<User>();

	private UserDatabase(){};
	
	public static String printlistUser() {
		String database = "######Users connected######\n";
		for(Iterator<User> it=userList.iterator(); it.hasNext();) {
			
			//On cré un user temp, parce que sinon a chaque appel de it.next() on passe a l'utilisateur suivant
			User temp = it.next();
			database += "# " + temp.pseudo + " : " + temp.adr.getHostAddress() + " #\n";
            //System.out.println(it.next().pseudo); 
		}
		database+="###########################\n";
		return database;
	}

	public static String printPseudos() {
		String database = "Pseudos connectés : ";
		for(User user : userList) {

			database += user.pseudo + ", ";
			//System.out.println(it.next().pseudo);
		}
		return database;
	}
	
	
	public static boolean updateDatabase(List<User> stack) {
		UserDatabase.userList = stack;
		return true;
	}
	
	/**
	 * Retourne un grâce a son pseudo
	 * @param pseudo
	 * @return temp 
	 */
	public static User getByPseudo(String pseudo) {
		for(Iterator<User> it=userList.iterator(); it.hasNext();) {
			User temp = it.next();
			if(pseudo.equals(temp.pseudo) ) {
				return temp;
			}
		
		}
		System.out.println("L'utilisateur n'existe pas !");
		return null;
	}
	

	public static boolean pseudoExist(String pseudo){
		for(User user : userList) {
			if(user.pseudo.equals(pseudo)) return true;
		}
		return false;
	}
	
	public static boolean adrExist(InetAddress adr) {
		
		for(Iterator<User> it=userList.iterator(); it.hasNext();) {
			if(adr.equals(it.next().adr) ) {
				return true;
			}
		}
		
		return false;
		
	}
	
	public static boolean userExist(User user) {
		if( (pseudoExist(user.pseudo)  || adrExist(user.adr)) == true) return false;
			
		return true;
		
	}

	public static void changePseudo(InetAddress ip, String newPseudo) {
		for(User user : userList) {
			if(user.adr.equals(ip)) user.pseudo = newPseudo;
		}
	}


	public static boolean  addUser(User user) {
		if(adrExist(user.adr) == true  ) {
			System.out.println("User "+user +" existe deja");
			changePseudo(user.adr,user.pseudo);
			return false;
		}

		System.out.println("Ajout de l'user " + user.pseudo +":" + user.adr);
		userList.add(user);
		return true;

	}

	public static User getByAdr(InetAddress inetAddress) {
		for(User user :userList){
			if(user.adr.equals(inetAddress)) return user;
		}
		return null;
	}
}
