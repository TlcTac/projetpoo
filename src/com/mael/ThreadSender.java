package com.mael;

import java.net.*;
import java.util.List;
import java.io.*;

public class ThreadSender {
	
	private String messageToSend;
	private User receiver;
	private int idSessionReceiver;
	PrintWriter out;
	Socket socketDest;
	
	public static int send(Message mes, InetAddress receiver) {
		
		
	      final Socket clientSocket;
	      if(receiver == null) return 0;
	      try {


			  clientSocket = new Socket(receiver, 5001);

			  if (mes.file == false) {

				  //flux pour envoyer un objet
				  ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());


				  Thread envoyer = new Thread(new Runnable() {
					  Message msg;

					  @Override
					  public void run() {
						  msg = mes;
						  try {
							  out.writeObject(mes);
							  out.flush();
							  if (msg.text != null) System.out.println("[ThreadSender] Envoi du message : " + msg.text);
						  } catch (IOException e) {
							  e.printStackTrace();
						  }

					  }
				  });
				  envoyer.start();
				  //clientSocket.close();

			  }

			  if (mes.file == true) {

				  //Stream vers le destinataire
				  ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());

				  Thread envoyer = new Thread(new Runnable() {
					  Message msg = mes;

					  @Override
					  public void run() {
						  msg = mes;

						  File file = new File(mes.text);
						  InputStream in = null;

						  try {
							  in = new FileInputStream(file);
						  } catch (FileNotFoundException e) {
							  e.printStackTrace();
						  }


						  // Get the size of the file
						  //TODO
						  //Si la taille du fichier est trop grande (5Mo ? ) ne pas envoyer le fichier
						  //Test effectué pour du .txt, .png pour 20octets a 5Mo sans probleme
						  int length = (int) file.length();
						  System.out.println(length);

						  //Remplir le tableau de bytes du message de la taille adéquate
						  msg.bytes = new byte[16 * length];


						  try {
							  //in.read retourne le nombre d'octets lu et place tout les octets lu depuis le stream in dans mes.bytes
							  //tant qu'il y a encore de la lecture on attends
							  while ((in.read(msg.bytes)) > 0) {
							  }
							  out.writeObject(msg);
							  out.flush();
							  System.out.println("[ThreadSender] Envoi du fichier : " + msg.text + "taille " + msg.bytes.length);
						  } catch (IOException e) {
							  e.printStackTrace();
						  }

					  }
				  });
				  envoyer.start();


			  }

		  }catch (java.net.ConnectException e) {
	      	System.out.println("Connexion avec l'adresse " + receiver + " impossible, l'user est peut etre déconnecté");
			  try {
				  HTTPNotifier.fetch();
			  } catch (IOException e1) {
				  e1.printStackTrace();
			  }
			  if(testGUI.GUIEnabled) {
				  testGUI.updateUser();
				  testGUI.chatFrame.setVisible(true);
			  }
		  } catch (IOException e) {
	           e.printStackTrace();
	      }
		return 1;
	}
		
}
