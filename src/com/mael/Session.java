package com.mael;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

class Session {

	public User userServer;
	public static int nbrSession;
	public int idSession;

	/**
	 * Liste des messages dans une session
	 * Ce sont des objets Messages et non des strings
	 * @see Message
	 */
	public List<Message> historiqueMessage = new ArrayList<Message>();
	
	
	/**
	 * Constructeur par défaut, incrémente de 1 le nbr de session
	 * @param userServer
	 */
	public Session(User userServer) {
		nbrSession +=1;
		this.userServer = userServer;
		this.idSession = nbrSession;
	}

	/**
	 * Retourne l'historique des messages dans une session
	 *  @return L'historique des messages dans un String
	 */
	public String showMessage(){
		String test;
		test="######## HISTORIQUE #######\n";
		for(Message mes : historiqueMessage) {
			test+= new SimpleDateFormat("dd-MM-YYYY HH:mm:ss").format(mes.timestamp) + " " + mes.text + "\n";

		}
		test+="###########################\n";
		return test;
	}


	public User getUserServer() {
		return userServer;
	}
	public void setUserServer(User userServer) {
		this.userServer = userServer;
	}
	public int getIdSession() {
		return this.idSession;
	}
	public void setIdSession(int idSession) {
		this.idSession = idSession;
	}	
}
