package com.mael;

//STEP 1. Import required packages
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SQLDB {

    // JDBC driver name and database URL
    //For embedded DB, we can use :
    //static final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    // With conn = DriverManager.getConnection("jdbc:derby:testdb1;create=true");
    // Ne pas oublier de creer alors manuellement les tables mentionnées après au premier lancement
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/clavardage?Unicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    //  Database credentials
    static String USER = "root";
    static String PASS = "root";

    static boolean DBSQLEnabled = true;

    //Daatabase connection
    static Connection conn = null;


    //Deux tables:
    /*

    On stock tout les users a qui l'ont a parlé avec leur dernier pseudo en date
    IPDest est une clé primaire, elle est unique, elle ne peut pas apparaitre deux fois a cause de l'hypothese
    qu'un utilisateur est sur un poste fixe et ne bouge jamais,  1IP = 1 user

    CREATE TABLE Users (
            IPDest VARCHAR(15),
            Pseudo VARCHAR(60),
            PRIMARY KEY (IPDest)
    );

    Correspondance entre IP et message
    CREATE TABLE Messages (
        IPDest VARCHAR(15),
        Message VARCHAR(500),
        Date DATETIME,
        FOREIGN KEY (IPDest) REFERENCES Users(IPDest)
    );


     */


    private SQLDB(){}

    public static void init(){

        //STEP 1: Register JDBC driver
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            DBSQLEnabled = false;
            System.out.println("Driver registration failed");
        }

        //STEP 2: Open a connection
        System.out.println("Connecting to database...");
        try {
            //conn = DriverManager.getConnection("jdbc:derby:derbyDB;create=true");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            DBSQLEnabled = false;
            System.out.println("getConnection() failed");
        }

    }



    /**
     *  INSERT INTO Messages (IPDest, Message,Date) VALUES (?,?,?)
     *  Ajoute un message dans la table Messages
     *  @param blabla Le message a ajouter
     *  @param ip L'adresse ip de l'utilisateur
     */
    public static void addMessage(String ip, Message blabla) {

        InetAddress iptemp = null;
        try {
            iptemp = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        addUser(iptemp, UserDatabase.getByAdr(iptemp).pseudo);

        String sql = "INSERT INTO Messages (IPDest, Message,Date)\n" +
                "VALUES (?,?,?)";
        try (PreparedStatement stm = conn.prepareStatement(sql)) {

            stm.setString(1, ip);
            stm.setString(2, UserDatabase.getByAdr(iptemp).pseudo + " : " + blabla.text);
            stm.setTimestamp(3, blabla.timestamp);
            stm.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    /**
     * Check si un user existe dans la DB SQL
     *
     * @param pseudo Le pseudo de l'utilisateur
     * @param ip     L'adresse ip de l'utilisateur
     * @return true si l'user existe, false sinon
     */
    public static boolean userExist(String ip, String pseudo) {
        List<User> users = new ArrayList<User>();
        users = getUsers();
        for (User user : users) {
            if (user.adr.getHostAddress().equalsIgnoreCase(ip)) {
                //Change le pseudo si il n'est pas le meme que dans la table user
                if (user.pseudo.equals(pseudo) == false) {
                    System.out.println("Le pseudo a changé, " + user.pseudo + " devient " + pseudo);
                    changePseudo(user.adr.getHostAddress(), pseudo);
                }
                return true;
            }
        }
        System.out.println("User n'existe pas dans DB SQL ");
        return false;
    }

    /**
     * UPDATE users SET Pseudo = ? WHERE  IPDest = ?
     * Change le pseudo de l'utilisateur qui a une certaine IP
     *
     * @param newPseudo Le nouveau pseudo de l'utilisateur
     * @param ip        L'adresse ip de l'utilisateur
     * @return int 0 si pas d'erreur
     */
    public static int changePseudo(String ip, String newPseudo) {
        String sql = "UPDATE users\n" +
                "SET Pseudo = ?" +
                "WHERE  IPDest = ?";

        try (PreparedStatement stm = conn.prepareStatement(sql)) {

            System.out.println(ip);
            stm.setString(1, newPseudo);
            stm.setString(2, ip);
            stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return 0;

    }


    /**
     * INSERT INTO Users (IPDest, Pseudo) VALUES (?,?)
     * Ajoute un nouvel utilisateur dans la table Users
     *
     * @param pseudo Le pseudo de l'utilisateur
     * @param adr    L'adresse ip de l'utilisateur
     * @return int 0 si pas d'erreur
     */
    public static int addUser(InetAddress adr, String pseudo) {
        if (userExist(adr.getHostAddress(), pseudo) == true) return -1;

        System.out.println("Ajout dans DB SQL de l'utilisateur " + pseudo + ":" + adr);
        String sql = "INSERT INTO Users (IPDest, Pseudo)\n" +
                "VALUES (?,?)";
        try (PreparedStatement stm = conn.prepareStatement(sql)) {

            stm.setString(1, adr.getHostAddress());
            stm.setString(2, pseudo);
            stm.executeUpdate();

            stm.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static String getTables(){
        String sql = "Show Tables";
        Statement stm = null;
        String tables ="";
        try {
            stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                //Retrieve by column name
                tables += (rs.getString("Tables_in_clavardage"));
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return tables;


    }



    /**
     * SELECT * FROM Users
     * Retourne la liste de tout les utilisateurs a qui l'ont a parlé
     *
     * @return List<User> retourne la liste des utilisateurs
     */
    public static List<User> getUsers() {
        String sql = "SELECT * FROM Users";
        Statement stm = null;
        List<User> userList = new ArrayList<User>();

        try {
            stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                //Retrieve by column name
                String ip = rs.getString("IPDest");
                String pseudo = rs.getString("Pseudo");
                User user = new User(pseudo, InetAddress.getByName(ip));
                //System.out.println("Recup de " + user.pseudo );
                userList.add(user);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


        return userList;

    }

    /**
     * SELECT * FROM Messages WHERE IPDest = ?
     * Selectionne tout les messages de la table Messages pour une certaine ip
     *
     * @param ip L'adresse ip
     * @return List<Message> retourne la liste des messsages
     */
    public static List<Message> getMessages(String ip) {

        String sql = "SELECT * FROM Messages WHERE IPDest = ?";
        ResultSet rs = null;
        List<Message> messageList = new ArrayList<Message>();

        try (PreparedStatement stm = conn.prepareStatement(sql)) {


            stm.setString(1, ip);
            rs = stm.executeQuery();
            while (rs.next()) {

                //Retrieve by column name
                String message = rs.getString("Message");
                Timestamp tmstp = rs.getTimestamp("Date");

                Message mes = new Message(message);
                mes.timestamp = tmstp;
                messageList.add(mes);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return messageList;
    }
}