@echo off
rd /s /q out
md out

@echo on
javac -cp ;lib/gson-2.8.5.jar;lib/servlet-api.jar;mysql-connector-java-8.0.13.jar src/com/mael/*.java -d out/
@echo off


@echo off
type banner.txt
cd out/
set /p choix=Client CLI ou GUI ? 
echo %choix%
if /I %choix%==CLI (
	echo OK CLI
	echo java -cp ;../lib/gson-2.8.5.jar;../lib/mysql-connector-java-8.0.13.jar com.mael.MainClient
	java -cp ;../lib/gson-2.8.5.jar;../lib/mysql-connector-java-8.0.13.jar com.mael.MainClient
) else if /I %choix%==GUI (
	echo OK GUI
	echo java -cp ;../lib/gson-2.8.5.jar;../lib/mysql-connector-java-8.0.13.jar com.mael.testGUI
	java -cp ;../lib/gson-2.8.5.jar;../lib/mysql-connector-java-8.0.13.jar com.mael.testGUI
) ELSE ( echo "Taper CLI ou GUI svp"
	pause)